<style>
.md-content__inner li a {
	color: white;
	background-color: #2196F3;
}
</style>

## *nix documentation

- **OpenBSD on Dell poweredge** : https://parksdigital.com/openbsd-on-dell-poweredge-r610.html
- **openbsd handbook** : https://www.openbsdhandbook.com/installation/
- **Why you should migrate everything from Linux to BSD** : https://www.unixsheikh.com/articles/why-you-should-migrate-everything-from-linux-to-bsd.html
- **Linuxjourney** (new to Linux ?) : https://linuxjourney.com/

## programming documentation

- **programming is terrible** : https://programmingisterrible.com/post/139222674273/write-code-that-is-easy-to-delete-not-easy-to
- **don't create chaos** : https://staysaasy.com/management/2020/07/07/dont-create-chaos.html
- **python and multipathlib for different os** : https://docs.python.org/3.7/library/pathlib.html
- **writing a filesystem in Rust** : https://blog.carlosgaldino.com/writing-a-file-system-from-scratch-in-rust.html

## hosting providers documentation

- **ungleich redmine** : https://redmine.ungleich.ch/projects/open-infrastructure/wiki
- **google peering and infrastructure** : https://peering.google.com/

## digest

- **perfect way to multiply** : https://www.quantamagazine.org/mathematicians-discover-the-perfect-way-to-multiply-20190411/
- **emergency remote : so you to work remotely ?** : https://www.emergencyremote.com/emergencyremote
