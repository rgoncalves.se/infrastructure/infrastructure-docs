Infrastructure management is handled through different git repositories,
internally and via public mirror hosted on https://gitlab.com/

Internal repositories | Public mirrors
--------------------- | --------------
<host\>:/srv/git       | https://gitlab.com/rgoncalves.se/infrastructure

## Ansible

Repository used for `Infrastructure as a Software`, handling new hosts and services setup.

The repository has been designed to handle servers and devices configurations through jinja2 templatings, thanks to the `template` built-in functionnaity in ansible.
One thing to remember as a tradeoff for managing `host variables`, is that we generate a bunch of files such as : `inventory.ini`, `host_var/*`, `group_vars/*`, etc ... through our builtin `infrastructure-generation` framework.
However, all variables related to servers and hosts have to be treated as depencies, and no direct editing of their values is allowed.
If so, please refer below to `infrastructure-generation`.

Public mirror |
------------- |
https://gitlab.com/rgoncalves.se/infrastructure/infrastructure-ansible |

## Generation

Public mirror |
------------- |
https://gitlab.com/rgoncalves.se/infrastructure/infrastructure-generation |

## Docs
