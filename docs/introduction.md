> For an up to date version of this introduction, please use the internal documentation.
> https://gitlab.com/rgoncalves.se/infrastructure/infrastructure-docs

## Infrastructure@<rgoncalves.se>

Infrastructure running at <rgoncalves.se>.

What it is and what it's not :

- {+ more code whem I'm bored OwO +}
- {- another boring, ready to use and semi working framework for wasting time with your infrastructure -}
- {+ another boring and not working framework, existing only for nuking all your servers +}

## Repositories

### [[ ansible ]]

[repository](https://gitlab.com/rgoncalves.se/infrastructure/infrastructure-ansible)

Repository for ansible scripts and templates and managing all hosts simultaneously.

### [[ generation ]]

[repository](https://gitlab.com/rgoncalves.se/infrastructure/infrastructure-generation)

Python module for generating all kind of templates and variables, according to one master file (ansible hosts, diagram vizualization, bash automation, ssh management, ...). Templating tools thanks to jinja2 !

### [[ docs ]]

[repository](https://gitlab.com/rgoncalves.se/infrastructure/infrastructure-docs)

Internal documentation, mostly used as a proof of concept and for keeping tracks of all changes.

## Vizualization

![](out/infrastructure_diagram.png)

## Credentials

No credentials are handled through public repositories.
Condiders all these git repositories as public mirrors.
