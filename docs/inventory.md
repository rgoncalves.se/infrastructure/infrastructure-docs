
> Generated from infrastucture-generation at [rgoncalves.se](rgoncalves.se),
> using jinja template.


Name | Comment | Operating System
-----|------|-----------------
	LP-graphite | Development and hacking laptop | archlinux
	WS-bentonite | Development workstation | archlinux
	PH-lineage | Development smartphone | archlinux


Name | Comment | IP | Ports | Operating System
-----|---------|----|------|------------------
	CHV-DC-rainbow|Domain controller with personnal server and wireguard|<li><strong style="color:red">local</strong> : --> 185.203.114.234</li><li><strong style="color:red">wg4</strong> : --> 10.10.0.1</li><li><strong style="color:red">wg6</strong> : --> fd00:10:10::1</li>|<li><strong style="color:red">22</strong> : --> ssh</li><li><strong style="color:red">51820</strong> : --> wireguard</li>|openbsd
	OOP-DV-stack0|Openbsd development station with virtual machines (lineage build server)|<li><strong style="color:red">local</strong> : --> 182.168.5.40</li><li><strong style="color:red">wg4</strong> : --> 10.10.0.40</li><li><strong style="color:red">wg6</strong> : --> fd00:10:10::40</li>|<li><strong style="color:red">22</strong> : --> ssh</li>|openbsd
	OOP-FS-raspberry|File system server for git and syncthing backup|<li><strong style="color:red">local</strong> : --> 192.168.5.41</li><li><strong style="color:red">wg4</strong> : --> 10.10.0.41</li><li><strong style="color:red">wg6</strong> : --> fd00:10:10::41</li>|<li><strong style="color:red">2244</strong> : --> ssh</li>|debian


Name | Comment | Ports | Operating System
-----|---------|-------|-----------------
	internet |  |  | 
	OOP-SW-01 | Basic network switch used in wooden lab mkI |  | 
	OOP-RT-54gl | Homelab router with wifi diffusion |  | 

