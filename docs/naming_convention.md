## Clients naming convention

???+ success "Scheme"
	| device type | nickname | (increment) |
	|-------------|----------|-------------|

=== "Device type"
	Which type of devices is this ?
	99% of the time it is a physical device :

	- `WS` --> Workstation
	- `LP` --> Laptop
	- `SP` --> Smartphone

=== "Nickname"
	Nicknames identify quickly the host you talk about.
	They are based on [minerals](https://en.wikipedia.org/wiki/List_of_minerals).

=== "Increment"
	Increments start from `00` to `99`.
	They are used in-case the whole identifier before is the same as an other server


## Servers and Components naming convention

???+ success "Scheme"
	| location | physical/virtual | role | nickname | (increment) |
	|----------|------------------|------|----------|-------------|

=== "Location"
	The location can be a **two uppercase characters country code** :

	- `FR` --> France
	- `CH` --> Swiss
	- `DE` --> Germany
	---
	- `OO` --> Origin (main server room)

=== "Physical / virtual"
	Describes the server type we have, can we touch it ? We even have a cluster indicator also :

	- `P` --> Physical
	- `V` --> Virtual
	- `C` --> Cluster
	
=== "Role"
	Which role(s) this server has ? Think primary role :

	- `DC` --> Domain controller (*vpn, dns, ...*)
	- `FS` --> File system (*syncthing, ftp, git, ...*)
	- `TR` --> Torrent (*torrent, *)
	- `WB` --> Web server (*lighttpd, grafana, *)
	- `DV` --> Development server (* *)
	- `RT` --> Router
	- `SW` --> Switch
	- `BK` --> Backup
	
=== "Nickname"
	Nicknames identify quickly the server you talk about.
	They are based on [colors](https://en.wikipedia.org/wiki/List_of_colors_(compact)) or eventually on [fruits](https://en.wikipedia.org/wiki/List_of_culinary_fruits).

	- `rainbow` --> i.e. domain controller
	- `raspberry` --> i.e. a real raspberry pi micro-computer

=== "Increment"
	Increments start from `00` to `99`.
	They are used in-case the whole identifier before is the same as an other server

