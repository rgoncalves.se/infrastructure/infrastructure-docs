Rules to follow when setting-up new devices on the network

- `router address` --> `192.168.5.1`
- `base address` --> `192.168.5.X`
- `netmask` --> `255.255.255.0`

Device type       | Range
------------------|--------
Network Equipment | *[* **00 <--> 09** *]*
Client            | *[* **20 <--> 39** *]*
<li>Server --> IPMI</li> | *[* **10 <--> 19** *]*
<li>Server --> physical</li> | *[* **40 <--> 60** *]*
<li>Server --> virtual</li> | *[* **60 <--> 99** *]*
DHCP              | *[* **100 <--> 255** *]*

